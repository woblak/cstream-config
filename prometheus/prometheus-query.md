## counts:  
increase(cstream_ingesterCryptowat_out_seconds_count[1m])
increase(cstream_ingester_out_seconds_count[1m])
increase(cstream_aggSummary_out_seconds_count[1m])
increase(cstream_aggMean_out_seconds_count[1m])
increase(cstream_sseUpdated_out_seconds_count[1m])

## time processing:  
cstream_ingesterCryptowat_out_seconds
cstream_ingester_out_seconds
cstream_aggSummary_out_seconds
cstream_aggMean_out_seconds
cstream_sseUpdated_out_seconds

## machine:  
process_cpu_usage
sum(jvm_memory_used_bytes) by (job)  
rate(jvm_gc_pause_seconds_sum[1m])  