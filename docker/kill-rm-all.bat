FOR /f "tokens=*" %%i IN ('docker ps -q') DO docker kill %%i

timeout 1

FOR /f "tokens=*" %%i IN ('docker ps -a -q') DO docker rm %%i
docker builder prune \y