Contains cluster configurations on docker.  
  
You can run the whole cstream application by docker-compose up on the yml files in the following order:  

1. docker-compose-kafka.yml  
2. docker-compose-ingester-cryptowat.yml  
3. docker-compose-ingester.yml  
4. docker-compose-aggregator.yml  
5. docker-compose-sse-provider-18201.yml & docker-compose-sse-provider-18202.yml  
6. docker-compose-sse-provider-loadbalancer.yml  
7. docker-compose-front.yml  
8. docker-compose-metrics.yml (optional)  
  
or if you are a windows user run the bat scripts in following order:  
1. up-kafka.bat  
2. up-ingester-cryptowat.bat  
3. up-ingester.bat  
4. up-aggregator.bat  
5. up-sse-provider.bat  
6. up-loadbalancer.bat
7. up-front.bat  
8. up-metrics.bat (optional)  